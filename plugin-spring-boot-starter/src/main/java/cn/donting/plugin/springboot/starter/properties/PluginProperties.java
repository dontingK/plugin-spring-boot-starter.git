package cn.donting.plugin.springboot.starter.properties;


import lombok.Data;
import lombok.Setter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.io.File;

/**
 * 插件的配置类
 */
@Data
@ConfigurationProperties(prefix = "cn.donting.plugin")
public class PluginProperties {

    private static PluginProperties pluginProperties;

    /**
     * 插件管理者 Id
     * 主程序Id
     * plugin Id =="-1" 则走 主程序
     *
     * @see cn.donting.plugin.springboot.starter.servlet.PluginMainDispatcherServlet
     */
    private String mainId = "main";

    /**
     * 插件的数据路径
     */
    private String dataPath = System.getProperty("user.dir") + File.separator + "plugins";

    /**
     * 插件的安装 db数据 目录名
     *  dataPath+ dbPathName
     */
    private String dbPathName ="db";


    /**
     * 插件的安装目录
     */
    private String pluginsPathName = "plugins";

    /**
     * 插件的 公共依赖包 路径
     * 该路径下的包需要每个插件都自己加载
     */
    private String pluginLibPathName ="pluginLib";


    public PluginProperties() {
        pluginProperties = this;
    }

    public String getDbPath() {
        return dataPath+File.separator+dbPathName;
    }

    public String getPluginsPath() {
        return dataPath+File.separator+pluginsPathName;
    }
    public String pluginLibPath() {
        return dataPath+File.separator+pluginLibPathName;
    }

    public static String pluginLibPaths(){
        return pluginProperties.pluginLibPath();
    }
}
