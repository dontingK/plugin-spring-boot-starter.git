package cn.donting.springboot.plugin.top;
/**
 * 插件的 容器上下文
 */
public interface PluginApplicationContext {
    /**
     * 获取 spring 的 中的 Environment Properties
     * @param key
     * @return
     */
    String getProperties(String key);

    /**
     * 获取对应 name 的类型
     * @param name bean name
     * @return Class
     */
    Class<?> getType(String name);

    /**
     * 根据 类型 class 获取spring容器中的 bean
     * @param requiredType 类型
     * @param <T> T
     * @return bean
     * @throws Exception
     */
    <T> T getBean(Class<T> requiredType) throws Exception;

    /**
     * 停止spring 容器
     * @return
     */
    int stopApplication();
}
