package cn.donting.plugin.springboot.starter.extension.lmp;

import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.exception.PluginNotFoundException;
import cn.donting.plugin.springboot.starter.extension.IPluginDispatcher;
import cn.donting.plugin.springboot.starter.extension.IPluginManager;
import cn.donting.plugin.springboot.starter.properties.PluginProperties;
import org.springframework.beans.factory.annotation.Autowired;

import javax.servlet.http.HttpServletRequest;

/**
 * 默认的  IPluginDispatcher
 * 取 query 中的 pluginId
 */
public class DefaultPluginDispatcher implements IPluginDispatcher {
    @Autowired
    IPluginManager pluginManager;

    @Autowired
    PluginProperties pluginProperties;

    @Override
    public String getPluginId(HttpServletRequest httpServletRequest) {
        String pluginId = httpServletRequest.getParameter("pluginId");
        return pluginId;
    }


}
