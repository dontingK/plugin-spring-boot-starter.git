package cn.donting.plugin.springboot.starter.condition;

import cn.donting.plugin.springboot.starter.env.PluginDevProperty;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.annotation.MergedAnnotation;
import org.springframework.core.annotation.MergedAnnotations;
import org.springframework.core.type.AnnotatedTypeMetadata;


/**
 * OnPluginCondition
 */
public class OnPluginCondition implements Condition {
    @Override
    public boolean matches(ConditionContext conditionContext, AnnotatedTypeMetadata metadata) {
        MergedAnnotations annotations = metadata.getAnnotations();
        MergedAnnotation<ConditionOnPluginEnv> evnConditionMergedAnnotation = annotations.get(ConditionOnPluginEnv.class);
        boolean dev = evnConditionMergedAnnotation.getBoolean("dev");
        String modeSrt = conditionContext.getEnvironment().getProperty(PluginDevProperty.ENABLED_DEV);
        boolean mode = Boolean.parseBoolean(modeSrt);
        if (dev && mode) {
            return true;
        }
        return false;
    }


}
