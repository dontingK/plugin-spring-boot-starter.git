package cn.donting.plugin.springboot.starter.extension;

import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;

import javax.servlet.ServletOutputStream;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * plugin Dispatcher 分发器
 *
 * @see cn.donting.plugin.springboot.starter.servlet.PluginMainDispatcherServlet
 */
public interface IPluginDispatcher {

    /**
     * 根据 HttpServletRequest 返回插件Id
     *
     * @param httpServletRequest
     * @return pluginId  返回null 则走主程序
     */
    String getPluginId(HttpServletRequest httpServletRequest);

    /**
     * 处理  PluginDispatcher 时出现的错误
     * @param ex
     * @param pluginId
     * @param req
     * @param res
     * @return
     */
    default void dispatcherError(Exception ex, String pluginId, ServletRequest req, ServletResponse res) throws IOException {
        HttpServletResponse response=  (HttpServletResponse)res;
        ServletOutputStream outputStream = response.getOutputStream();
        response.setStatus(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        response.setHeader(HttpHeaders.CONTENT_TYPE, "application/json;charset=UTF-8");
        outputStream.write(ex.getMessage().getBytes(StandardCharsets.UTF_8));
    }
    /**
     * 未 安装插件 返回的信息
     *
     * @param pluginId
     * @return
     */
    default String notInstallPluginMsg(String pluginId) {
        return "未安装插件id为" + pluginId + "插件";
    }

    /**
     * 插件 不是web 插件返回的信息
     *
     * @param pluginInfo
     * @return
     */
    default String pluginNotWebMsg(PluginInfo pluginInfo) {
        return "插件 " + pluginInfo.getName() + "[" + pluginInfo.getId() + "] 不是一个 web 插件";
    }

    /**
     * pluginId 参数为空
     *
     * @return
     */
    default String pluginIdNUll() {
        return "plugin Id 不能为空";
    }

    /**
     * pluginId 参数为空
     *
     * @return
     */
    default String pluginRunError(String pluginId) {
        return "插件启动失败："+pluginId;
    }

    default void before(HttpServletRequest httpServletRequest){};

    default void after(HttpServletRequest httpServletRequest,PluginApplication pluginApplication){};

}
