package cn.donting.plugin.springboot.starter.configuration;

import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.condition.ConditionOnPluginEnv;
import cn.donting.plugin.springboot.starter.env.PluginDevProperty;
import cn.donting.plugin.springboot.starter.extension.IPluginManager;
import cn.donting.plugin.springboot.starter.extension.PluginInfoDb;
import cn.donting.plugin.springboot.starter.loader.PluginLoader;
import cn.donting.plugin.springboot.starter.loader.dev.BootDevPluginLoader;
import cn.donting.plugin.springboot.starter.loader.dev.BootWebDevPluginLoader;
import cn.donting.plugin.springboot.starter.properties.PluginProperties;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * dev 加载器
 * 负责在模式加载 开发中的插件
 */
@Configuration
@ConditionOnPluginEnv(dev = true)
@EnableConfigurationProperties(PluginDevProperty.class)
@Slf4j
public class DevLoaderAutoConfiguration implements ApplicationRunner {

    @Autowired
    PluginDevProperty pluginDevProperty;

    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    PluginProperties pluginProperties;
    @Autowired
    PluginInfoDb pluginInfoDb;
    @Autowired
    IPluginManager pluginManager;


    @Bean
    public BootDevPluginLoader bootDevPluginLoader() {
        return new BootDevPluginLoader();
    }

    @Bean
    public BootWebDevPluginLoader bootWebDevPluginLoader() {
        return new BootWebDevPluginLoader();
    }


    /**
     *
     * @param args
     * @throws Exception
     */
    @Override
    public void run(ApplicationArguments args) throws Exception {


        String dir = pluginDevProperty.getDir();
        File pluginDir = new File(dir);
        File[] files = pluginDir.listFiles();
        ArrayList<File> devs = new ArrayList();
        for (File file : files) {
            if (file.getName().endsWith(".classpath.dev")) {
                log.info("检测到dev plugin:"+file.getName());
                devs.add(file);
            }
        }
        List<String> pluginUpdateIds = new ArrayList<>();
        for (File file : devs) {
            PluginLoader pluginLoader = IPluginManager.getPluginLoader(applicationContext, file);
            PluginApplication plugin = pluginLoader.loader(file);
            PluginInfo PluginInfo = pluginInfoDb.get(plugin.getPluginInfo().getId());
            if (PluginInfo == null) {
                log.info("安转dev插件：" + plugin.getPluginInfo());
                //第一次 安装
                PluginInfo install = pluginManager.install(file).getPluginInfo();
                pluginUpdateIds.add(install.getId());
            }
            //已安装 则更新
            if (PluginInfo != null) {

                //本次更新的 插件Id
                pluginUpdateIds.add(plugin.getPluginInfo().getId());
                pluginManager.updatePlugin(file,false);
            }
        }
        List<PluginInfo> all = pluginInfoDb.getAll();
        //检查 dev插件在本次是否更新，否 则卸载
        for (PluginInfo PluginInfo : all) {
            if (pluginUpdateIds.contains(PluginInfo.getId())) {
                continue;
            }
            if (PluginInfo.getInstallFileName().endsWith(".classpath.dev")) {
                //是dev 则卸载 说明本次未加载该dev
                pluginManager.unInstall(PluginInfo.getId());
            }
        }
    }

}
