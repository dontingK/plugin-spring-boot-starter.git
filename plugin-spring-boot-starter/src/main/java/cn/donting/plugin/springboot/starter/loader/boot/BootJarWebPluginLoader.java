package cn.donting.plugin.springboot.starter.loader.boot;

import cn.donting.plugin.springboot.starter.application.BootWebApplication;
import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import cn.donting.plugin.springboot.starter.servlet.PluginServletContext;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

/**
 * springboot web jar 加载器
 *
 * @author donting
 */
@Slf4j
public class BootJarWebPluginLoader extends BootJarPluginLoader {

    @Override
    public boolean isLoader(File file) {
        try {
            if (!isSpringBootJar(file)) {
                return false;
            }
            JarFile jarFile = new JarFile(file);
            Enumeration<JarEntry> entries = jarFile.entries();
            while (entries.hasMoreElements()) {
                JarEntry jarEntry = entries.nextElement();
                if (jarEntry.getName().contains("spring-web")) {
                    return true;
                }
            }
            return false;
        } catch (Exception ex) {
            log.warn(ex.getMessage());
            return false;
        }
    }


    @Override
    protected PluginApplication creatPluginApplication(PluginURLClassLoader classLoader, Class<?> mainClass, PluginInfo pluginInfo) {
        PluginApplication pluginApplication = new BootWebApplication(classLoader, new PluginServletContext(), mainClass, pluginInfo);
        return pluginApplication;
    }
}
