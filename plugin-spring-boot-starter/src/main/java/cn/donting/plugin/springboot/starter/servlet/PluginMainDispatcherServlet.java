package cn.donting.plugin.springboot.starter.servlet;


import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.extension.lmp.DefaultPluginManager;
import cn.donting.plugin.springboot.starter.extension.IPluginDispatcher;
import cn.donting.plugin.springboot.starter.properties.PluginProperties;
import cn.donting.springboot.plugin.top.web.PluginWebDispatcher;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.servlet.DispatcherServlet;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * 用于分发http请求
 * 该类 中不能注入 httpServlet 相关，此时httpServlet 还未绑定到线程中
 *
 * @author donting
 * @see org.springframework.web.servlet.FrameworkServlet#processRequest
 * 2021-05-30 下午2:41
 */
@Slf4j
public class PluginMainDispatcherServlet extends DispatcherServlet {

    @Autowired
    IPluginDispatcher pluginDispatcher;

    @Autowired
    DefaultPluginManager pluginManager;

    /**
     * 主程序Id
     */
    private final String mainId;

    public PluginMainDispatcherServlet(PluginProperties pluginProperties) {
        logger.info("PluginMainDispatcherServlet ......");
        mainId = pluginProperties.getMainId();
    }

    @Override
    public void service(ServletRequest req, ServletResponse res) throws ServletException, IOException {
        pluginDispatcher.before((HttpServletRequest) req);
        String pluginId = pluginDispatcher.getPluginId((HttpServletRequest) req);

        if (pluginId == null) {
            pluginDispatcher.dispatcherError(new PluginException("pluginId 为 null"),pluginId,req,res);
            return;
        }

        //主程序
        if (pluginId.equals(mainId)) {
            super.service(req, res);
            return;
        }

        PluginApplication plugin = null;
        try {
            plugin = pluginManager.getRunPlugin(pluginId);

        } catch (PluginException e) {
            logger.error(e.getCause(), e);
            pluginDispatcher.dispatcherError(e,pluginId,req,res);
            return;
        }

        //未安装的插件
        if (plugin == null) {
            pluginDispatcher.dispatcherError(new PluginException(pluginDispatcher.notInstallPluginMsg(pluginId)),pluginId,req,res);
            return;
        }
        pluginDispatcher.after((HttpServletRequest) req,plugin);
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        try {
            if (plugin instanceof PluginWebDispatcher) {
                //设置线程 上下文的类加载器
                Thread.currentThread().setContextClassLoader(plugin.getPluginURLClassLoader());
                ((PluginWebDispatcher) plugin).doService(req, res);
            } else {
                //插件不是一个 web 插件
                pluginDispatcher.dispatcherError(new PluginException(pluginDispatcher.pluginNotWebMsg(plugin.getPluginInfo())),pluginId,req,res);
            }
            return;
        } finally {
            //还原线程类加载器
            Thread.currentThread().setContextClassLoader(contextClassLoader);
        }

    }

}
