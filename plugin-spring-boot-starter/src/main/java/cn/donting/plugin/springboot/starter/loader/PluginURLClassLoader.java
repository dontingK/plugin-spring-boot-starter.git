package cn.donting.plugin.springboot.starter.loader;

import cn.donting.plugin.springboot.starter.properties.PluginProperties;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;

/**
 * 插件的类加载器
 */
public class PluginURLClassLoader extends URLClassLoader {
    /**
     * 插件的信息
     */
    private PluginInfo pluginInfo;
    /**
     * 插件的name
     */
    private String name;

    /**
     *
     * @param urls  类路径 urls
     * @param parent 父加载器
     */
    public PluginURLClassLoader(URL[] urls, ClassLoader parent) {
        super(urls, parent);
        File file = new File(PluginProperties.pluginLibPaths());
        for (File listFile : file.listFiles()) {
            try {
                addURL(listFile.toURI().toURL());
            } catch (MalformedURLException e) {
               throw new RuntimeException(e);
            }
        }
    }

    /**
     * 设置 插件信息
     * @param pluginInfo
     */
    public void setPluginInfo(PluginInfo pluginInfo) {
        this.pluginInfo = pluginInfo;
        this.name = pluginInfo.getName();
    }

    /**
     * 获取插件信息
     * @return
     */
    public PluginInfo getPluginInfo() {
        return pluginInfo;
    }
}
