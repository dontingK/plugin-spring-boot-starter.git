package cn.donting.plugin.springboot.starter.event;

import cn.donting.plugin.springboot.starter.entity.PluginInfo;

public class AfterUpdateEvent extends PluginEvent {
    public AfterUpdateEvent(PluginInfo pluginInfo) {
        super(pluginInfo);
    }
}
