package cn.donting.plugin.springboot.starter.exception;

/**
 * 插件未支持异常类
 */
public class PluginNotSupportException extends PluginRuntimeException {
    public PluginNotSupportException() {
        super("未支持!!!");
    }

    public PluginNotSupportException(String message) {
        super(message);
    }

    public PluginNotSupportException(String message, Throwable cause) {
        super("未支持!!!", cause);
    }
}
