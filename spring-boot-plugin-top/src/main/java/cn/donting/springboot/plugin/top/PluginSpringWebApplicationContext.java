package cn.donting.springboot.plugin.top;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;

/**
 * spring web 容器上下文
 * @author donting
 */
public interface PluginSpringWebApplicationContext extends PluginSpringApplicationContext,PluginWebApplicationContext {
   ServletContext getServletContext();
   ServletConfig getServletConfig();
}
