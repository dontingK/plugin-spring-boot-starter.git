package cn.donting.plugin.springboot.starter.loader;

import org.springframework.util.FileCopyUtils;

import java.io.*;

/**
 * 抽象的 dev 插件加载器
 */
public abstract class PluginDevLoader extends PluginLoader {

    @Override
    public boolean isLoader(File file) {
        if (file.exists() && file.getName().endsWith(".classpath.dev")) {
            return true;
        }
        return false;
    }

}
