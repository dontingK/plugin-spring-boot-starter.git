package cn.donting.springboot.plugin.run;


import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.HandlesTypes;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.*;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;
import java.util.jar.Manifest;


public class WarMain implements ServletContainerInitializer {


    @Override
    public void onStartup(Set<Class<?>> c, ServletContext ctx) throws ServletException {
        String realPath = ctx.getRealPath("/");
        String replacement = File.separator;
        if (File.separator.equals("\\")) {
            replacement = "\\\\";
        }
        File file = new File(realPath + File.separator + JarFile.MANIFEST_NAME.replaceAll("/", replacement));
        try {
            Manifest mf = new Manifest(new FileInputStream(file));
            String startClass = mf.getMainAttributes().getValue("Start-Class");
            ArrayList<URL> classpath = new ArrayList<>();
            File lib = new File(realPath + File.separator + "BOOT-INF" + File.separator + "lib");
            classpath.add(new File(realPath + File.separator + "BOOT-INF" + File.separator + "classes").toURI().toURL());
            for (File listFile : lib.listFiles()) {
                classpath.add(listFile.toURI().toURL());
            }
            PluginMainClassLoader pluginMainClassLoader = new PluginMainClassLoader(classpath.toArray(new URL[0])
                    , Thread.currentThread().getContextClassLoader());
            Thread.currentThread().setContextClassLoader(pluginMainClassLoader);
            ServiceLoader<ServletContainerInitializer> load = ServiceLoader.load(ServletContainerInitializer.class, pluginMainClassLoader);

            ArrayList<ServletContainerInitializer> initializers = new ArrayList();
            Iterator<ServletContainerInitializer> iterator = load.iterator();
            while (iterator.hasNext()) {
                ServletContainerInitializer next = iterator.next();
                if (next.getClass().getClassLoader() == pluginMainClassLoader) {
                    initializers.add(next);
                }
            }
            for (ServletContainerInitializer initializer : initializers) {
                HandlesTypes ht = initializer.getClass().getDeclaredAnnotation(HandlesTypes.class);
                if(ht==null){
                    initializer.onStartup(new HashSet<>(),ctx);
                }else{
                    Class<?>[] value = ht.value();
                    HashSet<Class<?>> impClass = ClassImp.isImp(pluginMainClassLoader, value);
                    initializer.onStartup(impClass,ctx);
                }
            }


        } catch (IOException e) {
            e.printStackTrace();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    public HashSet<Class<?>> getImpClass(URLClassLoader urlClassLoader, Class<?>[] classes) throws URISyntaxException, IOException, ClassNotFoundException {
        URL[] urLs = urlClassLoader.getURLs();
        HashSet<Class<?>> impClasses = new HashSet<>();
        HashSet<Class<?>> interfaceClass=new HashSet<>();
        for (Class<?> aClass : classes) {
            impClasses.add(aClass);
        }
        for (URL urL : urLs) {
            String path = urL.toURI().getPath();
            File file = new File(path);
            getImpClass(file.getPath(),file,interfaceClass,impClasses,urlClassLoader);
        }

        return impClasses;

    }


    private void getImpClass(String prPath, File file, HashSet<Class<?>> interfaceClass, HashSet<Class<?>> c, URLClassLoader urlClassLoader) throws ClassNotFoundException, IOException {
        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File file1 : files) {
                getImpClass(prPath, file1, interfaceClass, c, urlClassLoader);
            }
        }
        if (file.isFile()) {
            if (file.getName().endsWith(".jar")) {
                JarFile jarFile = new JarFile(file);
                Enumeration<JarEntry> entries = jarFile.entries();
                while (entries.hasMoreElements()) {
                    JarEntry jarEntry = entries.nextElement();
                    String className = jarEntry.getName().replaceAll("/", ".");
                    if (className.endsWith(".class")&&!className.equals("module-info.class")) {
                        isImp(interfaceClass, c, urlClassLoader, className.substring(0,className.length()-6));
                    }

                }
            }
            if (file.getName().endsWith(".class")&& !file.getName().endsWith("module-info.class")) {
                String name = file.getPath();
                name = replacePath(name.substring(prPath.length() + 1,name.length()-6),".");
                isImp(interfaceClass, c, urlClassLoader, name);
            }
        }
    }

    private void isImp(HashSet<Class<?>> interfaceClass, HashSet<Class<?>> c, URLClassLoader urlClassLoader, String name) throws ClassNotFoundException {
       try {
           Class<?> aClass = urlClassLoader.loadClass(name);
           Class<?>[] interfaces = aClass.getInterfaces();
           for (Class<?> anInterface : interfaces) {

               if (interfaceClass.contains(anInterface)) {
                   c.add(anInterface);
               }
           }
       }catch (Throwable ex){
           System.err.println(name+"::"+ex.getMessage());
       }
    }

    private String replacePath(String s,String replacement) {
        String regex=File.separator;
        if(File.separator.equals("\\")){
            regex="\\\\";
        }
        return s.replaceAll(regex, replacement);
    }


}
