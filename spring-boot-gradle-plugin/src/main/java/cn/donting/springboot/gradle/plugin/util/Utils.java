package cn.donting.springboot.gradle.plugin.util;

import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;
import org.springframework.boot.gradle.plugin.ResolveMainClassName;

import java.io.File;
import java.lang.reflect.Method;
import java.util.Iterator;
import java.util.List;

public class Utils {
    public static String springbootMainClass(Project project) throws Exception {
        try {
            ResolveMainClassName bootRunMainClassName = (ResolveMainClassName) project.getTasks().getByName("bootRunMainClassName");
            Method resolveMainClassNameMethod = ResolveMainClassName.class.getDeclaredMethod("resolveMainClassName");
            resolveMainClassNameMethod.setAccessible(true);
            Object invoke = resolveMainClassNameMethod.invoke(bootRunMainClassName);
            return invoke.toString();
        } catch (ClassCastException | NoSuchMethodException ex) {
            System.err.println("检查是否使用  plugin: org.springframework.boot>=2.4.0");
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }
    public static String springbootClasspath(Project project,String split) throws Exception {
        try {
            StringBuilder stringBuilder = new StringBuilder();
            ResolveMainClassName bootRunMainClassName = (ResolveMainClassName) project.getTasks().getByName("bootRunMainClassName");
            FileCollection classpath = bootRunMainClassName.getClasspath();
            Iterator<File> iterator = classpath.iterator();
            while (iterator.hasNext()) {
                File file = iterator.next();
                stringBuilder.append(file.getPath()+split);
            }
            return stringBuilder.toString().substring(0,stringBuilder.length()-split.length());
        } catch (ClassCastException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static String listToString(List list, String split){
        String str="";
        for (int i = 0; i < list.size(); i++) {
            if(i==list.size()-1){
                str+=list.get(i).toString();
            }else {
                str+=list.get(i).toString()+split;
            }
        }
        return str;
    }

    public static String arrayToString(Object[] list,String split){
        String str="";
        for (int i = 0; i < list.length; i++) {
            if(i==list.length-1){
                str+=list[i].toString();
            }else {
                str+=list[i].toString()+split;
            }
        }
        return str;
    }
}
