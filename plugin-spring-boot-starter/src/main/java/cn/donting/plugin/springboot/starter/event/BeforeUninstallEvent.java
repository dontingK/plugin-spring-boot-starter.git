package cn.donting.plugin.springboot.starter.event;

import cn.donting.plugin.springboot.starter.entity.PluginInfo;

public class BeforeUninstallEvent extends PluginEvent {
    public BeforeUninstallEvent(PluginInfo pluginInfo) {
        super(pluginInfo);
    }
}
