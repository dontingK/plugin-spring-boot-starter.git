package cn.donting.plugin.main.test;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.web.servlet.ServletWebServerFactoryAutoConfiguration;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextClosedEvent;

@SpringBootApplication
public class TestPluginApplication implements ApplicationListener<ContextClosedEvent> {
    public static void main(String[] args) {
        SpringApplication.run(TestPluginApplication.class,args);
    }

    @Override
    public void onApplicationEvent(ContextClosedEvent applicationEvent) {
        System.out.println(applicationEvent);
        System.out.println("ContextClosedEvent");
        System.out.println("ContextClosedEvent");
        System.out.println("ContextClosedEvent");
        System.out.println("ContextClosedEvent");
    }
}
