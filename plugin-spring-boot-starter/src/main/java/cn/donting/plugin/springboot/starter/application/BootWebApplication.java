package cn.donting.plugin.springboot.starter.application;

import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.springboot.plugin.top.PluginApplicationContext;
import cn.donting.springboot.plugin.top.PluginSpringWebApplicationContext;
import cn.donting.springboot.plugin.top.PluginSpringWebRunner;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import java.io.IOException;

/**
 * springboot web 的 Application
 */
public class BootWebApplication extends PluginWebApplication {
    /**
     * servletContext 不是原始servlet 容器的servletContext
     * @see cn.donting.plugin.springboot.starter.servlet.PluginServletContext
     */
    private ServletContext servletContext;

    public BootWebApplication(PluginURLClassLoader pluginURLClassLoader, ServletContext servletContext, Class<?> mainClass, PluginInfo pluginInfo) {
        super(pluginURLClassLoader, mainClass, pluginInfo);
        this.servletContext = servletContext;
    }

    @Override
    public PluginApplicationContext run(String[] args) throws Exception {
        if (super.pluginApplicationContext != null) {
            new PluginException("app is running");
        }
        ClassLoader contextClassLoader = Thread.currentThread().getContextClassLoader();
        Thread.currentThread().setContextClassLoader(pluginURLClassLoader);
        Class<?> pluginApplicationRunnerClass = pluginURLClassLoader.loadClass("cn.donting.plugin.springboot.starter.plugin.PluginSpringWebApplicationRunner");
        PluginSpringWebRunner pluginRunner = (PluginSpringWebRunner) pluginApplicationRunnerClass.newInstance();
        PluginApplicationContext pluginApplicationContext = pluginRunner.run(mainClass, servletContext, new String[]{});
        super.pluginApplicationContext = pluginApplicationContext;
        Thread.currentThread().setContextClassLoader(contextClassLoader);
        return pluginApplicationContext;
    }


    @Override
    public int stop() throws Exception {
        try {
            return pluginApplicationContext.stopApplication();
        } catch (Exception exception) {
            throw exception;
        }
    }

    @Override
    public void doService(ServletRequest httpRequest, ServletResponse httpResponse) throws ServletException, IOException {
        PluginSpringWebApplicationContext pluginApplicationContext = (PluginSpringWebApplicationContext) this.pluginApplicationContext;
        pluginApplicationContext.doService(httpRequest, httpResponse);
    }
}
