package cn.donting.plugin.springboot.starter.event;

import cn.donting.plugin.springboot.starter.entity.PluginInfo;

public class AfterInstallEvent extends PluginEvent {
    public AfterInstallEvent(PluginInfo pluginInfo) {
        super(pluginInfo);
    }
}
