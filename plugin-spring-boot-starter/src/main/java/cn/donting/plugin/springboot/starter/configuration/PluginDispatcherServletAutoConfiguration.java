package cn.donting.plugin.springboot.starter.configuration;

import cn.donting.plugin.springboot.starter.properties.PluginProperties;
import cn.donting.plugin.springboot.starter.servlet.PluginMainDispatcherServlet;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.autoconfigure.condition.*;
import org.springframework.boot.autoconfigure.web.servlet.WebMvcProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.DispatcherServlet;
import javax.servlet.ServletRegistration;

import static org.springframework.boot.autoconfigure.web.servlet.DispatcherServletAutoConfiguration.DEFAULT_DISPATCHER_SERVLET_BEAN_NAME;

/**
 *  PluginDispatcherServlet 配置类
 */
@Configuration(proxyBeanMethods = false)
@AutoConfigureOrder(Ordered.HIGHEST_PRECEDENCE)
@ConditionalOnClass(ServletRegistration.class)
@EnableConfigurationProperties(WebMvcProperties.class)
@Slf4j
public class PluginDispatcherServletAutoConfiguration {
    public PluginDispatcherServletAutoConfiguration() {
        log.info("PluginDispatcherServletAutoConfiguration init ....");
    }

    /**
     * 覆盖 spring 默认的  DispatcherServlet
     * @param webMvcProperties
     * @return
     */
    @Bean(name = DEFAULT_DISPATCHER_SERVLET_BEAN_NAME)
    public DispatcherServlet dispatcherServlet(WebMvcProperties webMvcProperties, PluginProperties pluginProperties) {
        PluginMainDispatcherServlet dispatcherServlet = new PluginMainDispatcherServlet(pluginProperties);
        dispatcherServlet.setDispatchOptionsRequest(webMvcProperties.isDispatchOptionsRequest());
        dispatcherServlet.setDispatchTraceRequest(webMvcProperties.isDispatchTraceRequest());
        dispatcherServlet.setThrowExceptionIfNoHandlerFound(webMvcProperties.isThrowExceptionIfNoHandlerFound());
        dispatcherServlet.setPublishEvents(webMvcProperties.isPublishRequestHandledEvents());
        dispatcherServlet.setEnableLoggingRequestDetails(webMvcProperties.isLogRequestDetails());
        return dispatcherServlet;
    }
//    @Bean
//    @ConditionalOnMissingBean(PluginController.class)
//    public PluginController pluginController(){
//        return new PluginController();
//    }

}
