package cn.donting.springboot.gradle.plugin.extension;


public class DevPlugin {
    String projectPath;
    String mainClass;

    public String getProjectPath() {
        return projectPath;
    }

    public void setProjectPath(String projectPath) {
        this.projectPath = projectPath;
    }

    public String getMainClass() {
        return mainClass;
    }

    public void setMainClass(String mainClass) {
        this.mainClass = mainClass;
    }

    @Override
    public String toString() {
        return "DevPlugin{" +
                "projectPath='" + projectPath + '\'' +
                ", mainClass='" + mainClass + '\'' +
                '}';
    }
}
