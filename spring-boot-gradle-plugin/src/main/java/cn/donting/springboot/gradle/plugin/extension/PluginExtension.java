package cn.donting.springboot.gradle.plugin.extension;

import org.gradle.api.Project;
import org.gradle.api.internal.tasks.DefaultSourceSetContainer;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.Property;

import java.io.File;
import java.util.Set;

public class PluginExtension {
    private final Project project;

    /**
     * 插件的 project
     */
    private final ListProperty<String>  pluginProjectPath;
    /**
     * 插件的 主类 （run使用）
     */
    private final ListProperty<String>  pluginMainClass;
    /**
     * 主程序文件 插件run使用
     */
    private final Property<File> mainFile;
    /**
     * 公共依赖
     * 建议不要使用 springboot starter ；否者多个spring.factories 会覆盖。
     * 不能 理解pom 依赖，需要自己添加子依赖
     * 主程序 打包 使用
     * task mainRun
     */
    private final ListProperty<String> commonLib;

    public PluginExtension(Project project) {
        this.project = project;
        this.pluginProjectPath = this.project.getObjects().listProperty(String.class);
        this.pluginMainClass = this.project.getObjects().listProperty(String.class);
        this.mainFile = this.project.getObjects().property(File.class);
        this.commonLib = this.project.getObjects().listProperty(String.class);
    }

    public ListProperty<String> getPluginProjectPath() {
        return pluginProjectPath;
    }

    public ListProperty<String> getPluginMainClass() {
        return pluginMainClass;
    }

    public Property<File> getMainFile() {
        return mainFile;
    }

    public ListProperty<String> getCommonLib() {
        return commonLib;
    }
}
