package cn.donting.plugin.springboot.starter.dev;

import cn.donting.plugin.springboot.starter.exception.PluginRuntimeException;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import lombok.Getter;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

/**
 * 插件启动的配置类
 */
@Getter
public class DevRunClass {
    /**
     * 插件的类路径
     */
    ArrayList<String> classpaths = new ArrayList<>();
    /**
     * 主类完整类路径
     */
    String mainClass;


    private DevRunClass(String mainClass) {
        this.mainClass = mainClass;
    }

    public File toFile(String filePath) throws IOException {
        File file = new File(filePath);
        file.getParentFile().mkdirs();
        file.createNewFile();
        FileOutputStream fileOutputStream = new FileOutputStream(file);
        fileOutputStream.write((mainClass + "\n").getBytes());
        for (int i = 0; i < classpaths.size() - 1; i++) {
            fileOutputStream.write((classpaths.get(i) + "\n").getBytes());
        }
        fileOutputStream.write(classpaths.get(classpaths.size()-1).getBytes());
        fileOutputStream.close();
        return file;
    }

    /**
     * 从 .classpath.dev 文件读取 为 DevRunClass
     * @param file 文件路径
     * @return  DevRunClass
     * @throws IOException
     */
    public static DevRunClass build(File file) throws IOException {
        //构造一个BufferedReader类来读取文件
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(file), "UTF-8"));
        String line = br.readLine();
        DevRunClass devRunClass = new DevRunClass(line);
        //使用readLine方法，一次读一行
        while ((line = br.readLine()) != null) {
            devRunClass.getClasspaths().add(line);
        }
        br.close();
        return devRunClass;

    }

    /**
     * 获取 PluginURLClassLoader
     * @return PluginURLClassLoader
     */
    public PluginURLClassLoader getPluginURLClassLoader() {
        try {
            ArrayList<URL> url = new ArrayList<URL>();
            for (String classpath : classpaths) {
                url.add(new File(classpath).toURI().toURL());
            }
            URL[] urls = url.toArray(new URL[0]);
            PluginURLClassLoader pluginURLClassLoader = new PluginURLClassLoader(urls, DevRunClass.class.getClassLoader().getParent());
            return pluginURLClassLoader;
        } catch (MalformedURLException e) {
            throw new PluginRuntimeException(e.getMessage(), e);
        }
    }
}