package cn.donting.plugin.springboot.starter.event;

import cn.donting.plugin.springboot.starter.entity.PluginInfo;

public class AfterUninstallEvent extends PluginEvent {

    public AfterUninstallEvent(PluginInfo pluginInfo) {
        super(pluginInfo);
    }
}
