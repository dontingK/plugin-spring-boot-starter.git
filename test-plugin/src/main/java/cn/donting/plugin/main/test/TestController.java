package cn.donting.plugin.main.test;


//import cn.donting.plugin.main.test.dao.UserRepository;
//import cn.donting.plugin.main.test.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
public class TestController {
    public  TestController() {
        System.out.println("TestController。。。。。。。。。。。。。。。。");
    }
//
//    @Autowired
//    UserRepository userRepository;
//
//    @GetMapping("/test")
//    public String test(){
//        return "testPlugin";
//    }
//
//    @GetMapping("/jpa")
//    public User jpa() {
//        User user = new User();
//        user.setEmail("setEmailPlugin");
//        user.setName("setEmailPlugin");
//        user=userRepository.save(user);
//        return user;
//    }

    @GetMapping("/html")
    public ModelAndView html(ModelAndView mv) {
        mv.setViewName("/testTh");
        mv.addObject("title","欢迎使用Thymeleaf!");
        return mv;
    }

}
