package cn.donting.plugin.springboot.starter.event;

import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import org.springframework.context.ApplicationEvent;

public class PluginEvent extends ApplicationEvent {
    private  final PluginInfo pluginInfo;
    public PluginEvent(PluginInfo pluginInfo) {
        super(pluginInfo);
        this.pluginInfo=pluginInfo;
    }

    public PluginInfo getPluginInfo() {
        return pluginInfo;
    }
}
