package cn.donting.springboot.gradle.plugin;

import org.springframework.boot.loader.tools.EntryWriter;
import org.springframework.boot.loader.tools.Library;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.jar.JarEntry;
import java.util.jar.JarInputStream;
import java.util.zip.CRC32;
import java.util.zip.ZipEntry;

public class PluginJarWriter extends org.springframework.boot.loader.tools.JarWriter {


    public PluginJarWriter(File file) throws IOException {
        super(file);
    }


    /**
     * 写入Library 文件,原封不动
     *
     * @param location 路径 BOOT-INF/lib/
     * @param library
     * @throws IOException
     */
    @Override
    public void writeNestedLibrary(String location, Library library) throws IOException {
        super.writeNestedLibrary(location, library);
    }

    /**
     * 复制一个jar 中的class 等...文件 (非内部jar)
     *
     * @param file
     * @throws IOException
     */
    public void copyForBooJar(File file) throws IOException {
        URL loaderJar = file.toURI().toURL();
        this.writeLoaderClassesNotJar(loaderJar);
    }

    /**
     * 写入 一个 jar 中的 非 .jar文件
     *
     * @param loaderJar
     * @throws IOException
     */
    public void writeLoaderClassesNotJar(URL loaderJar) throws IOException {
        try (JarInputStream inputStream = new JarInputStream(new BufferedInputStream(loaderJar.openStream()))) {
            JarEntry entry;
            while ((entry = inputStream.getNextJarEntry()) != null) {
                if (entry.getName().endsWith(".jar")) {
                    continue;
                }
                writeEntry(entry.getName(), new InputStreamEntryWriter(inputStream));
            }
        }
    }

    /**
     * 写入 一个 jar 中的 文件
     *
     * @param loaderJar
     * @throws IOException
     */
    public void writeLoaderClasses(URL loaderJar, WriteName<JarEntry> writeName) throws IOException {
        try (JarInputStream inputStream = new JarInputStream(new BufferedInputStream(loaderJar.openStream()))) {
            JarEntry entry;
            while ((entry = inputStream.getNextJarEntry()) != null) {
                String name = writeName.getWriteName(entry);
                if (name != null) {
                    writeEntry(name, new InputStreamEntryWriter(inputStream));
                }

            }
        }
    }


    /**
     * 写入 一个 jar 中的 非 .jar文件
     *
     * @param loaderJar
     * @throws IOException
     */
    public void writeLoaderClasses(String location, URL loaderJar) throws IOException {
        try (JarInputStream inputStream = new JarInputStream(new BufferedInputStream(loaderJar.openStream()))) {
            JarEntry entry;
            while ((entry = inputStream.getNextJarEntry()) != null) {
                if (entry.getName().endsWith(".jar")) {
                    continue;
                }
                writeEntry(location + entry.getName(), new InputStreamEntryWriter(inputStream));
            }
        }
    }

    public void writeURL(String name, URL url) throws IOException {
        InputStreamEntryWriter inputStreamEntryWriter = new InputStreamEntryWriter(url.openStream());
        writeEntry(name, inputStreamEntryWriter);
    }


    private static class InputStreamEntryWriter implements EntryWriter {
        private static final int BUFFER_SIZE = 32 * 1024;

        private final InputStream inputStream;

        InputStreamEntryWriter(InputStream inputStream) {
            this.inputStream = inputStream;
        }

        @Override
        public void write(OutputStream outputStream) throws IOException {
            byte[] buffer = new byte[BUFFER_SIZE];
            int bytesRead;
            while ((bytesRead = this.inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, bytesRead);
            }
            outputStream.flush();
        }
    }


    public interface WriteName<T> {
        String getWriteName(T t) throws IOException;
    }

}
