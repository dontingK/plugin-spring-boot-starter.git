package cn.donting.plugin.springboot.starter.entity;


import lombok.Data;

/**
 * 插件信息
 */
@Data
public class PluginInfo {
    /**
     * 插件的name
     */
    private String name;
    /**
     * 插件的数字版本
     * 升级比对需要
     */
    private int numberVersion;
    /**
     * 插件可读 的版本
     * v1.0
     */
    private String version;
    /**
     * id 插件Id。正式加载的 插件Id.
     */
    private String id;

    /**
     * 安装后的路径
     */
    private String installFileName;
    /**
     * 安装 时间戳
     */
    private Long installTime;
    /**
     * 更新时间戳
     */
    private Long updateTime;

}
