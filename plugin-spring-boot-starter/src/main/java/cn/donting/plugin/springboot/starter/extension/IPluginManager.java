package cn.donting.plugin.springboot.starter.extension;

import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.loader.PluginLoader;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import org.springframework.context.ApplicationContext;

import java.io.File;
import java.util.Map;

/**
 * 插件管理者
 */
public interface IPluginManager {
    /**
     * 从文件安装插件
     *
     * @param file
     */
    PluginApplication install(File file) throws PluginException;

    /**
     * 卸载插件
     *
     * @param pluginId 插件Id
     */
    void unInstall(String pluginId) throws PluginException;

    /**
     * 启动插件
     *
     * @param pluginId pluginId
     */
    PluginApplication startPlugin(String pluginId) throws PluginException;

    /**
     * 停止插件
     *
     * @param pluginId pluginId
     * @return 退出码
     */
    int stopPlugin(String pluginId) throws PluginException;

    /**
     * 更新插件
     *
     * @param file    文件
     * @param version 是否校验版本 true 将不会更新 更新低版本
     */
    PluginApplication updatePlugin(File file, boolean version) throws PluginException;

    default PluginApplication updatePlugin(File file) throws PluginException {
        return updatePlugin(file, true);
    }


    /**
     * 获取运行中的插件
     *
     * @param pluginId pluginId
     * @return null 未运行插件
     */
    PluginApplication getRunningPlugin(String pluginId);


    /**
     * 获取并 运行插件
     * 插件未运行则 运行插件
     *
     * @param pluginId
     * @return null 未安装插件
     */
    default PluginApplication getRunPlugin(String pluginId) throws PluginException {
        if (isRunning(pluginId)) {
            return getRunningPlugin(pluginId);
        } else {
            if (getPluginInfo(pluginId) != null) {
                return startPlugin(pluginId);
            } else {
                return null;
            }
        }
    }

    /**
     * 插件是否运行
     *
     * @param pluginId
     * @return
     */
    boolean isRunning(String pluginId);


    /**
     * 获取插件信息
     *
     * @param pluginId
     * @return
     */
    PluginInfo getPluginInfo(String pluginId);

    /**
     * 根据文件获取相应的插件加载器
     *
     * @param file
     * @return
     * @throws PluginException 未找到加载的插件的加载器
     */
    static PluginLoader getPluginLoader(ApplicationContext applicationContext, File file) throws PluginException {
        Map<String, PluginLoader> pluginLoaders = applicationContext.getBeansOfType(PluginLoader.class);
        for (Map.Entry<String, PluginLoader> stringPluginLoaderEntry : pluginLoaders.entrySet()) {
            PluginLoader pluginLoader = stringPluginLoaderEntry.getValue();
            if (pluginLoader.isLoader(file)) {
                return pluginLoader;
            }
        }
        throw new PluginException(file.getPath() + ":找不到对应的插件加载器");
    }

}
