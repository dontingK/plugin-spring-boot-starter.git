package cn.donting.springboot.plugin.run;

import java.io.File;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

public class Main {
    public static void main(String[] args) throws MalformedURLException, ClassNotFoundException, InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        ClassLoader classLoader = Main.class.getClassLoader();
        Args arg = new Args(args);
        List<String> classpath = arg.getClasspath();
        URL[] urls = new URL[classpath.size()];
        for (int i = 0; i < classpath.size(); i++) {
            urls[i]=new File(classpath.get(i)).toURI().toURL();
        }
        PluginMainClassLoader pluginMainClassLoader=new PluginMainClassLoader(urls);
        Thread.currentThread().setContextClassLoader(pluginMainClassLoader);
        Class<?> mainClass = pluginMainClassLoader.loadClass(arg.getMainClass());
        Method main = mainClass.getMethod("main", String[].class);
        main.invoke(null,(Object) arg.getPluginMainArgs());

    }
}
