package cn.donting.springboot.plugin.top;

import javax.servlet.ServletContext;

/**
 * 插件 springboot web  Runner
 */
public interface PluginSpringWebRunner {
    /**
     * 启动 SpringWeb 容器
     * @param mainClass 主类
     * @param servletContext servletContext  servlet上下文
     * @param args  启动参数
     * @return 插件 上下文
     * @throws Exception 启动异常
     */
    PluginApplicationContext run(Class<?> mainClass, ServletContext servletContext, String... args) throws  Exception;
}
