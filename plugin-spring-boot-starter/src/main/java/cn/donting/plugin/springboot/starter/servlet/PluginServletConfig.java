package cn.donting.plugin.springboot.starter.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import java.util.Enumeration;

public class PluginServletConfig implements ServletConfig {
    ServletContext servletContext;

    public PluginServletConfig(ServletContext servletContext) {
        this.servletContext = servletContext;
    }

    public PluginServletConfig() {
    }

    @Override
    public String getServletName() {
        return "AppServletConfig";
    }

    @Override
    public ServletContext getServletContext() {
        return servletContext;
    }

    @Override
    public String getInitParameter(String name) {
        return null;
    }

    @Override
    public Enumeration<String> getInitParameterNames() {
        return new Enumeration<String>() {
            @Override
            public boolean hasMoreElements() {
                return false;
            }

            @Override
            public String nextElement() {
                return null;
            }
        };
    }

}
