package cn.donting.plugin.springboot.starter.plugin;

import cn.donting.plugin.springboot.starter.plugin.web.PluginAnnotationConfigServletWebServerApplicationContext;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.web.reactive.context.AnnotationConfigReactiveWebServerApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import javax.servlet.ServletContext;

/**
 * SpringApplication
 * 生成  PluginAnnotationConfigServletWebServerApplicationContext
 * @see cn.donting.plugin.springboot.starter.plugin.web.PluginAnnotationConfigServletWebServerApplicationContext
 * @author donting
 */
public class PluginSpringApplication extends SpringApplication {

    private ServletContext servletContext;


    public PluginSpringApplication(ServletContext servletContext,Class<?>... primarySources) {
        super(primarySources);
        this.servletContext=servletContext;
        setApplicationContextFactory(webApplicationType -> {
            try {
                switch (webApplicationType) {
                    case SERVLET:
                        //Servlet 容器 生成  对应的 plugin 的 spring web 容器
                        PluginAnnotationConfigServletWebServerApplicationContext pluginAnnotationConfigServletWebServerApplicationContext = new PluginAnnotationConfigServletWebServerApplicationContext();
                        return pluginAnnotationConfigServletWebServerApplicationContext;
                    case REACTIVE:
                        return new AnnotationConfigReactiveWebServerApplicationContext();
                    default:
                        return new AnnotationConfigApplicationContext();
                }
            } catch (Exception var2) {
                throw new IllegalStateException("Unable create a default ApplicationContext instance, you may need a custom ApplicationContextFactory", var2);
            }
        });
    }
}
