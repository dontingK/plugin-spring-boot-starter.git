package cn.donting.springboot.gradle.plugin.task;

import cn.donting.springboot.gradle.plugin.extension.PluginExtension;
import cn.donting.springboot.gradle.plugin.util.Utils;
import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.internal.file.collections.FileCollectionAdapter;
import org.gradle.api.internal.tasks.DefaultSourceSetContainer;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.JavaExec;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskAction;

import java.io.File;
import java.net.MalformedURLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class PluginRun extends JavaExec {


    @Override
    public void exec() {
        PluginExtension pe = getProject().getExtensions().findByType(PluginExtension.class);
        Property<File> mainFileProperty = pe.getMainFile();
        File mainFile = mainFileProperty.getOrElse(null);
        if (mainFile == null) {
            System.err.println("请先设置 pluginStarter.mainFile");
            throw new RuntimeException("请先设置 pluginStarter.mainFile");
        }
        System.out.println(mainFile);
        List<String> args = null;
        try {
            args = creatArgs();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
        setArgs(args);
        classpath(mainFile.toURI().getPath());
        super.exec();

    }


    public List<String> creatArgs() throws Exception {
        ArrayList<String> args = new ArrayList<>();
        args.add(PluginMainRun.OPEN_DEV);
        args.add(PluginMainRun.getPluginDevDir(getProject()));
        return args;
    }


    public static File[] runtimeClasspath(Project project) {
        DefaultSourceSetContainer sourceSets = (DefaultSourceSetContainer) project.getExtensions().getByName("sourceSets");
        SourceSet main = sourceSets.getByName("main");
        Set<File> files = main.getRuntimeClasspath().getFiles();
        return files.toArray(new File[0]);

    }
}
