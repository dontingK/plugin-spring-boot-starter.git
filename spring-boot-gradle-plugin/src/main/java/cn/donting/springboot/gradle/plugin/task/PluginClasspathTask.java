package cn.donting.springboot.gradle.plugin.task;

import org.gradle.api.DefaultTask;
import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.TaskAction;
import org.springframework.boot.gradle.plugin.ResolveMainClassName;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.nio.charset.StandardCharsets;
import java.util.Iterator;

/**
 * 来源于  org.springframework.boot:spring-boot-gradle-plugin:2.5.0
 * @author donting
 */
public class PluginClasspathTask extends DefaultTask {


    public PluginClasspathTask() {
    }

    @TaskAction
    public void classPath() {
        Project project = getProject();
        try {
            String classpath = classpath(project);
            File file = new File(project.getRootDir() + File.separator + project.getName() + ".classpath.dev");
            if (!file.exists()) {
                file.createNewFile();
            }
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(classpath.getBytes(StandardCharsets.UTF_8));
            fileOutputStream.flush();
            fileOutputStream.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    public static String classpath(Project project) throws Exception {
        try {

            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append(springbootMainClass(project));
            ResolveMainClassName bootRunMainClassName = (ResolveMainClassName) project.getTasks().getByName("bootRunMainClassName");
            FileCollection classpath = bootRunMainClassName.getClasspath();
            Iterator<File> iterator = classpath.iterator();
            while (iterator.hasNext()) {
                File file = iterator.next();
                stringBuilder.append("\n" + file.getPath());
            }
            return stringBuilder.toString();
        } catch (ClassCastException | NoSuchMethodException ex) {
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }
    }

    public static String springbootMainClass(Project project) throws Exception {
        try {

        ResolveMainClassName bootRunMainClassName = (ResolveMainClassName) project.getTasks().getByName("bootRunMainClassName");
        Method resolveMainClassNameMethod = ResolveMainClassName.class.getDeclaredMethod("resolveMainClassName");
        resolveMainClassNameMethod.setAccessible(true);
        Object invoke = resolveMainClassNameMethod.invoke(bootRunMainClassName);
        return invoke.toString();
        } catch (ClassCastException | NoSuchMethodException ex) {
            System.err.println("检查是否使用  plugin: org.springframework.boot >=2.4.0 ");
            throw ex;
        } catch (Exception ex) {
            throw ex;
        }

    }



}
