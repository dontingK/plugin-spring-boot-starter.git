package cn.donting.plugin.springboot.starter.extension.lmp;

import cn.donting.plugin.springboot.starter.extension.PluginInfoDb;
import cn.donting.plugin.springboot.starter.exception.PluginRuntimeException;
import cn.donting.plugin.springboot.starter.properties.PluginProperties;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class JsonPluginInfoDb implements PluginInfoDb {
    private String filePath;
    private final ObjectMapper objectMapper = new ObjectMapper();

    public JsonPluginInfoDb(PluginProperties properties) {
        filePath = properties.getDbPath() + File.separator + "pluginInfoDb.json";
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                DbInfo dbInfo = new DbInfo();
                saveDbInfo(dbInfo);
                file.createNewFile();
            } catch (Exception e) {
                log.error(e.getMessage(), e);
            }
        }
    }

    @Override
    public synchronized PluginInfo save(PluginInfo pluginInfo) {
        try {
            DbInfo dbInfo = getDbInfo();
            HashMap<String, PluginInfo> plugins = dbInfo.plugins;
            if (plugins.containsKey(pluginInfo.getId())) {
                throw new PluginRuntimeException(pluginInfo.getId() + " 已经安装");
            }
            plugins.put(pluginInfo.getId(), pluginInfo);
            saveDbInfo(dbInfo);
            return pluginInfo;
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }

    @Override
    public synchronized PluginInfo delete(String pluginId) {
        try {
            DbInfo dbInfo = getDbInfo();
            HashMap<String, PluginInfo> plugins = dbInfo.plugins;
            PluginInfo remove = plugins.remove(pluginId);
            saveDbInfo(dbInfo);
            return remove;
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }

    @Override
    public synchronized PluginInfo get(String pluginId) {
        try {
            DbInfo dbInfo = getDbInfo();
            HashMap<String, PluginInfo> plugins = dbInfo.plugins;
            return plugins.get(pluginId);
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }

    @Override
    public synchronized PluginInfo update(PluginInfo pluginInfo) {
        try {
            DbInfo dbInfo = getDbInfo();
            HashMap<String, PluginInfo> plugins = dbInfo.plugins;
            PluginInfo remove = plugins.remove(pluginInfo.getId());
            plugins.put(pluginInfo.getId(), pluginInfo);
            saveDbInfo(dbInfo);
            return pluginInfo;
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }

    @Override
    public synchronized ArrayList<PluginInfo> getAll() {
        try {
            Map<String, PluginInfo> plugins = getAllMap();
            ArrayList<PluginInfo> pluginInfos = new ArrayList<>();
            for (Map.Entry<String, PluginInfo> stringPluginInfoEntry : plugins.entrySet()) {
                PluginInfo PluginInfo = stringPluginInfoEntry.getValue();
                pluginInfos.add(PluginInfo);
            }
            return pluginInfos;
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }

    private synchronized Map<String, PluginInfo> getAllMap() {
        try {
            DbInfo dbInfo = getDbInfo();
            HashMap<String, PluginInfo> plugins = dbInfo.getPlugins();
            return plugins;
        } catch (Exception e) {
            throw new PluginRuntimeException(e);
        }
    }


    private final DbInfo getDbInfo() throws Exception {
        File dbFile = new File(filePath);
        DbInfo dbInfo = objectMapper.readValue(dbFile, DbInfo.class);
        return dbInfo;
    }

    private final void saveDbInfo(DbInfo dbInfo) throws Exception {
        File dbFile = new File(filePath);
        String jsonDb = objectMapper.writeValueAsString(dbInfo);
        FileOutputStream fileOutputStream=new FileOutputStream(dbFile);
        fileOutputStream.write(jsonDb.getBytes());
        fileOutputStream.close();
    }


    @Data
    private static class DbInfo {
        public final int version = 100;
        private HashMap<String, PluginInfo> plugins = new HashMap();
    }
}
