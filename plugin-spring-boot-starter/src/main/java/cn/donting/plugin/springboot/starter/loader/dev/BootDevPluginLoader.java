package cn.donting.plugin.springboot.starter.loader.dev;

import cn.donting.plugin.springboot.starter.application.BootApplication;
import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.dev.DevRunClass;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.loader.PluginDevLoader;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

/**
 *dev springboot jar 加载器 (非web)
 *
 * @author donting
 */
@Slf4j
public class BootDevPluginLoader extends PluginDevLoader {

    @Override
    public boolean isLoader(File file) {
        if (super.isLoader(file)) {
            try {
                DevRunClass build = DevRunClass.build(file);
                for (String classpath : build.getClasspaths()) {
                    if (classpath.contains("spring-webmvc")) {
                        return false;
                    }
                }
                return true;
            } catch (IOException e) {
                log.error("file:" + file);
                log.error(e.getMessage(), e);
                return false;
            }
        }
        return false;
    }

    @Override
    public PluginApplication loader(File file) throws PluginException {
        try {
            DevRunClass devRunClass = DevRunClass.build(file);
            PluginURLClassLoader pluginURLClassLoader = devRunClass.getPluginURLClassLoader();
            Class<?> aClass = pluginURLClassLoader.loadClass(devRunClass.getMainClass());
            PluginInfo pluginInfo = getPluginInfo(aClass, pluginURLClassLoader,file);
            //给 开发中的 插件name 加上 开发标识
            pluginInfo.setName(pluginInfo.getName()+"(开发)");
            PluginApplication pluginApplication = creatPluginApplication(pluginURLClassLoader,aClass,pluginInfo);
            return pluginApplication;
        } catch (IOException | ClassNotFoundException e) {
            throw new PluginException(e.getMessage(), e);
        }
    }

    @Override
    protected PluginApplication creatPluginApplication(PluginURLClassLoader classLoader, Class<?> mainClass, PluginInfo pluginInfo) {
        return new BootApplication(classLoader, mainClass, pluginInfo);
    }


}
