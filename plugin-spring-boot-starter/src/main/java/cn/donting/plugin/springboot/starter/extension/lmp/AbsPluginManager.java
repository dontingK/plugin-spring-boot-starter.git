package cn.donting.plugin.springboot.starter.extension.lmp;

import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.extension.IPluginManager;
import cn.donting.plugin.springboot.starter.extension.PluginInfoDb;
import cn.donting.plugin.springboot.starter.loader.PluginLoader;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;

import java.io.File;
import java.util.HashMap;

/**
 * 插件管理
 */
public abstract class AbsPluginManager implements IPluginManager {
    private Logger log = LoggerFactory.getLogger(AbsPluginManager.class);

    /**
     * 运行中的插件
     */
    protected HashMap<String, PluginApplication> runPlugins = new HashMap<>();


    @Autowired
    ApplicationContext applicationContext;

    @Autowired
    private PluginInfoDb pluginInfoDb;

    /**
     * 获取运行中的插件
     *
     * @param pluginId pluginId
     * @return PluginApplication
     */
    @Override
    public PluginApplication getRunningPlugin(String pluginId) {
        return runPlugins.get(pluginId);
    }

    /**
     * 插件是否运行
     *
     * @param pluginId
     * @return
     */
    @Override
    public boolean isRunning(String pluginId) {
        return runPlugins.containsKey(pluginId);
    }

    /**
     * 根据文件获取相应的插件加载器
     *
     * @param file
     * @return
     * @throws PluginException 未找到加载的插件的加载器
     */
    public PluginLoader getPluginLoader(File file) throws PluginException {
        return IPluginManager.getPluginLoader(applicationContext, file);
    }

    @Override
    public PluginInfo getPluginInfo(String pluginId) {
        PluginInfo pluginInfo = pluginInfoDb.get(pluginId);
        return pluginInfo;
    }

    /**
     * 加载Dev 的插件
     *
     * @param pluginApplication
     * @throws Exception
     */
    public void loadDevPlugin(PluginApplication pluginApplication) throws Exception {
        log.info("加载devPlugin name[" + pluginApplication.getPluginInfo().getName() + "] id:[{}]", pluginApplication.getPluginInfo().getId());
        pluginApplication.run(null);
        runPlugins.put(pluginApplication.getPluginInfo().getId(), pluginApplication);
    }

}
