package cn.donting.plugin.springboot.starter.extension;

import cn.donting.plugin.springboot.starter.entity.PluginInfo;

import java.util.List;

/**
 * 用于保存  插件安装信息的 db 类
 */
public interface PluginInfoDb<T extends PluginInfo> {
    /**
     * 保存插件安装信息
     * @param pluginInfo
     * @return PluginInfo
     */
    T save(T pluginInfo);

    /**
     * 删除插件安装信息
     * @param pluginId 插件Id
     * @return
     */
    T delete(String pluginId);

    /**
     * 获取插件安装信息
     * @param pluginId 插件Id
     * @return null
     */
    T get(String pluginId);

    /**
     * 更新插件安装信息
     * @param PluginInfo PluginInfo
     * @return
     */
    T update(T PluginInfo);

    /**
     * 获取所有的 插件安装信息
     * @return
     */
    List<T> getAll();

}
