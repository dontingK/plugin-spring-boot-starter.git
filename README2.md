pluginLib 所有插件都依赖的 PluginClassLoader 加载
plugin-lib-plugin-sdk-spring-boot-starter-1.1.0.3-SNAPSHOT.jar

1. spring-boot-gradle-plugin 中的Extension

   使用示例：pluginStarter.key="value"

   1). PluginExtension
        

| key               | 类型   | 说明                               |
| ----------------- | ------ | ---------------------------------- |
| pluginProjectPath | String | 插件的 project(":plugin-test") (主程序有效,用于在启动时自动加载插件)          |
| pluginMainClass   | String | 插件的 主类 （run使用,主程序有效） |
| mainFile          | File   | 主程序文件 插件run使用 (插件有效)  |
| commonLib         | String | 公共依赖（主程序有效）             |

2. gradle task

task在 plugin下组下

1）主程序

    (1) pluginMainRun ：启动主程序
    
    (2) pluginMainJar ：主程序打包

   2）插件

    (1) pluginMainRun  ：从主程序启动 插件，需要定义 pluginStarter.mainFile=new File("./main.jar")。 
    
    (2) pluginClasspath：生成.classpath.dev 文件用于 主程序加载插件依赖