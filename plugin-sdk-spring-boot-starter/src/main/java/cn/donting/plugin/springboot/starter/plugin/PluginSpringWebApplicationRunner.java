package cn.donting.plugin.springboot.starter.plugin;

import cn.donting.springboot.plugin.top.PluginApplicationContext;
import cn.donting.springboot.plugin.top.PluginSpringWebRunner;
import cn.donting.springboot.plugin.top.PluginSpringWebApplicationContext;

import javax.servlet.ServletContext;

/**
 * SpringWebApplicationRunner
 * spring web 启动器
 */
public class PluginSpringWebApplicationRunner implements PluginSpringWebRunner {
    @Override
    public PluginApplicationContext run(Class<?> mainClass, ServletContext servletContext, String... args) throws Exception {
        PluginSpringBootServletInitializer pluginSpringBootServletInitializer=new PluginSpringBootServletInitializer(mainClass,args);
        PluginSpringWebApplicationContext start = pluginSpringBootServletInitializer.start(servletContext);
        return start;
    }


//    @Override
//    public PluginApplicationContext run(Class<?> mainClass, ServletContext servletContext, String... args) {
//        PluginSpringApplication pluginSpringApplication=new PluginSpringApplication(servletContext,mainClass);
//        ConfigurableApplicationContext run = pluginSpringApplication.run(args);
//        return (PluginApplicationContext)run;
//    }
}
