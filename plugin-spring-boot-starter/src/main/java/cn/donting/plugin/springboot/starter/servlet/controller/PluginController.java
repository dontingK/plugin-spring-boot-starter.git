package cn.donting.plugin.springboot.starter.servlet.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//
///**
// * PluginMainDispatcherServlet 错误绑定
// * @see cn.donting.plugin.springboot.starter.servlet.PluginMainDispatcherServlet
// */
//@RestController
//@RequestMapping("/plugin")
//public class PluginController {
//
//    /**
//     * PluginMainDispatcherServlet 中分发错误路径
//     * @see cn.donting.plugin.springboot.starter.servlet.PluginMainDispatcherServlet
//     */
//    public static final String DISPATCHER_ERROR="/plugin/dispatcher/500";
//    /**
//     * 插件404
//     * @param msg 返回的信息
//     * @param httpCode httpCode 返回的 httpCode
//     */
//    @RequestMapping("/dispatcher/500")
//    public HttpEntity plugin500(String msg,Integer httpCode) {
//        HttpStatus httpStatus;
//        try {
//            httpStatus= HttpStatus.valueOf(httpCode);
//        }catch (Exception e){
//            httpStatus=HttpStatus.INTERNAL_SERVER_ERROR;
//        }
//        ResponseEntity httpEntity = new ResponseEntity(msg,httpStatus);
//        return httpEntity;
//    }
//
//}
