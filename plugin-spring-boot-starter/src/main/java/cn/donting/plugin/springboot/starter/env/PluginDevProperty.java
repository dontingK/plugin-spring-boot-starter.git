package cn.donting.plugin.springboot.starter.env;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 插件run 时的 启用
 * 插件 dev 模式的 配置想想
 */
@Data
@ConfigurationProperties(prefix = "cn.donting.plugin.dev")
public class PluginDevProperty {

    /**
     * 是否开启dev
     * 用于OnPluginCondition
     */
    public static final String ENABLED_DEV = "cn.donting.plugin.dev.enabled";
    /**
     * 是否开启dev
     */
    private boolean enabled = false;

    /**
     * 插件的  *.classpath.dev 的目录dir
     * 默认是 rootProject.dir
     */
    private String dir;


}
