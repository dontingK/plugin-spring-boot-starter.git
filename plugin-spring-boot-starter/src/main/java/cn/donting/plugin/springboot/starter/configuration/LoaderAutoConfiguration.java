package cn.donting.plugin.springboot.starter.configuration;

import cn.donting.plugin.springboot.starter.extension.IPluginDispatcher;
import cn.donting.plugin.springboot.starter.extension.IPluginInfoLoader;
import cn.donting.plugin.springboot.starter.extension.lmp.DefaultPluginDispatcher;
import cn.donting.plugin.springboot.starter.extension.lmp.DefaultPluginInfoLoader;
import cn.donting.plugin.springboot.starter.loader.boot.BootJarPluginLoader;
import cn.donting.plugin.springboot.starter.loader.boot.BootJarWebPluginLoader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * 插件加载相关的配置类
 */
@Configuration
@Slf4j
public class LoaderAutoConfiguration {


    public LoaderAutoConfiguration() {

    }

    @Bean
    public BootJarPluginLoader bootJarPluginLoader() {
        return new BootJarPluginLoader();
    }

    @Bean
    public BootJarWebPluginLoader bootJarWebPluginLoader() {
        return new BootJarWebPluginLoader();
    }

    @Bean
    @ConditionalOnMissingBean(IPluginDispatcher.class)
    public IPluginDispatcher pluginDispatcher() {
        return new DefaultPluginDispatcher();
    }

    @Bean
    @ConditionalOnMissingBean(IPluginInfoLoader.class)
    public IPluginInfoLoader pluginInfoLoader() {
        return new DefaultPluginInfoLoader();
    }


}
