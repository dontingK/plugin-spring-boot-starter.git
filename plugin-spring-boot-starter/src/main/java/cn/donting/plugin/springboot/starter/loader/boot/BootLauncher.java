package cn.donting.plugin.springboot.starter.loader.boot;

import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import org.springframework.boot.loader.JarLauncher;
import org.springframework.boot.loader.archive.Archive;

import java.net.URL;
import java.util.Iterator;


/**
 * 使 JarLauncher 生成 PluginURLClassLoader
 * @see org.springframework.boot.loader.JarLauncher
 * @see BootLauncher#createClassLoader(URL[])
 * @author donting
 */
public class BootLauncher extends JarLauncher {

    public BootLauncher(Archive  jarFileArchive) {
        super(jarFileArchive);
    }


    public PluginURLClassLoader createAppClassLoader() throws Exception {
        Iterator<Archive> classPathArchivesIterator = getClassPathArchivesIterator();
        PluginURLClassLoader classLoader = (PluginURLClassLoader)createClassLoader(classPathArchivesIterator);
        return classLoader;
    }

    @Override
    protected PluginURLClassLoader createClassLoader(URL[] urls) throws Exception {
        return new BootLauncherURLClassLoader(isExploded(), getArchive(), urls, getClass().getClassLoader().getParent());
    }

    @Override
    public String getMainClass() throws Exception {
        return super.getMainClass();
    }


    @Override
    protected boolean isNestedArchive(Archive.Entry entry) {
        return super.isNestedArchive(entry);
    }

    @Override
    public void launch(String[] args) throws Exception {
        super.launch(args);
    }
}
