package cn.donting.springboot.plugin.top;

import java.lang.annotation.Annotation;

/**
 * 插件的spring 容器上下文
 * @see org.springframework.context.support.AbstractApplicationContext
 */
public interface PluginSpringApplicationContext extends PluginApplicationContext {
    /**
     *Find all names of beans which are annotated with the supplied Annotation type, without creating corresponding bean instances yet.
     * Note that this method considers objects created by FactoryBeans, which means that FactoryBeans will get initialized in order to determine their object type.
     * Params:
     * annotationType – the type of annotation to look for (at class, interface or factory method level of the specified bean)
     * Returns:
     * the names of all matching beans
     * Since:
     * 4.0
     * See Also:
     * findAnnotationOnBea
     * @param annotationType
     * @return
     */
    String[] getBeanNamesForAnnotation(Class<? extends Annotation> annotationType);
}
