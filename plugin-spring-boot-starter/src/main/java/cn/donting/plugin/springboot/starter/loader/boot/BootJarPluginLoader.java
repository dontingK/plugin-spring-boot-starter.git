package cn.donting.plugin.springboot.starter.loader.boot;

import cn.donting.plugin.springboot.starter.application.BootApplication;
import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.loader.PluginLoader;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.loader.archive.JarFileArchive;

import java.io.File;
import java.io.IOException;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;

/**
 * springboot jar 加载器 (非web)
 *
 * @author donting
 */
@Slf4j
public class BootJarPluginLoader extends PluginLoader {



    @Override
    public boolean isLoader(File file) {
        return false;

    }

    @Override
    public PluginApplication loader(File file) throws PluginException {
        try {
            JarFileArchive jarFileArchive = new JarFileArchive(file);
            BootLauncher bootLauncher = new BootLauncher(jarFileArchive);
            PluginURLClassLoader pluginURLClassLoader = bootLauncher.createAppClassLoader();
            String mainClass = bootLauncher.getMainClass();
            Class<?> aClass = pluginURLClassLoader.loadClass(mainClass);
            PluginInfo pluginInfo = getPluginInfo(aClass, pluginURLClassLoader,file);
            if (pluginInfo == null) {
                throw new PluginException("未能生成 pluginInfo");
            }
            pluginURLClassLoader.setPluginInfo(pluginInfo);
            return creatPluginApplication(pluginURLClassLoader, aClass, pluginInfo);
        } catch (PluginException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new PluginException(ex);
        }
    }

    /**
     * 创建 PluginApplication
     *
     * @param classLoader 类加载器
     * @param mainClass   主类
     * @param pluginInfo  插件信息
     * @return
     */
    protected PluginApplication creatPluginApplication(PluginURLClassLoader classLoader, Class<?> mainClass, PluginInfo pluginInfo) {
        PluginApplication pluginApplication = new BootApplication(classLoader, mainClass, pluginInfo);
        return pluginApplication;
    }


    /**
     * 是否是springboot jar
     *
     * @param file
     * @return
     */
    public final static boolean isSpringBootJar(File file) {
        try {
            JarFile jarFile = new JarFile(file);
            ZipEntry entry = jarFile.getEntry("BOOT-INF/");
            if (entry != null) {
                jarFile.close();
                return true;
            }
            jarFile.close();
            return false;
        } catch (IOException e) {
            log.error(file.getPath() + " not SpringBootJar  ");
            log.error(e.getMessage(), e);
        }
        return false;
    }

    public static void main(String[] args) {
        File file = new File("D:\\新建文件夹\\plugin-spring-boot-starter\\test-plugin\\build\\libs\\test-plugin-0.0.0.1-plain.jar");
        boolean springBootJar = isSpringBootJar(file);
        System.out.println(springBootJar);
    }

}
