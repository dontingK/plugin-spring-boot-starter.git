package cn.donting.plugin.springboot.starter.loader.dev;

import cn.donting.plugin.springboot.starter.application.BootWebApplication;
import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.dev.DevRunClass;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.plugin.springboot.starter.servlet.PluginServletContext;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;

/**
 * dev springboot jar 加载器 (非web)
 *
 * @author donting
 */
@Slf4j
public class BootWebDevPluginLoader extends BootDevPluginLoader {
    @Override
    public boolean isLoader(File file) {
        if (file.exists() && file.getName().endsWith(".classpath.dev")) {
            try {
                DevRunClass build = DevRunClass.build(file);
                for (String classpath : build.getClasspaths()) {
                    if (classpath.contains("spring-webmvc")) {
                        return true;
                    }
                }
                return false;
            } catch (IOException e) {
                log.error("file:" + file);
                log.error(e.getMessage(), e);
                return false;
            }
        }
        return false;
    }

    @Override
    protected PluginApplication creatPluginApplication(PluginURLClassLoader classLoader, Class<?> mainClass, PluginInfo pluginInfo) {
        return new BootWebApplication(classLoader, new PluginServletContext(), mainClass, pluginInfo);
    }
}
