### plugin-spring-boot-starter 插件式开发框架
    让你的springboot 动态加载springboot。
## 核心功能：
+ 主程序完整的springboot支持
+ 主程序支持war打包
+ 插件动态热拔插
+ 不止能加载spring boot，还能加载自定义的插件模式。
+ 主程序，插件隔离，可用不同版本的Springboot
+ 扩展性强，预留扩展接口
+ 无需改造插件，只需要在resource下新增plugin.json文件，即可被主程序加载（当然你可以自定义 插件标识，例：文件名，插件主类上定义注解.....）
+ 插件独立性，插件不需要任何依赖，按照springboot打包为jar后即可被加载，也可独立运行。（不破环插件打包后的结构）
+ 插件几乎完整的spring-boot-starter支持
+ 提供gradle插件，用于开发需要。（maven插件暂未支持）
## 与springboot差异
 插件不支持jmx
## 插件支持（目前只发现spring-boot-starter-web  会有差异）
+  spring-boot-starter-web 
  差异：暂不支持servlet组件（后续会支持），如：Filter，Servlet...。其他mvc，拦截器（可代替Filter），静态资源均支持。
+ spring-boot-starter-thymeleaf(经测试支持)
+ spring-boot-starter-data-jpa(经测试支持)
+ mybatis-spring-boot-starter(经测试支持)
+ mybatis-plus-boot-starter(经测试支持)
+ ...................
+ ...................(理论支持所有的starter)

##使用
1.在 插件,主程序 build.gradle 中加入
```groovy
buildscript {
   repositories {
      maven {
      url "https://oss.sonatype.org/content/repositories/snapshots/"
}
mavenLocal()
}
dependencies {
      classpath "cn.donting:plugin-spring-boot-strater-gralde-plugin:1.2.0.1-SNAPSHOT"
}
}
```
2. 主程序中引入依赖
```groovy
   
   implementation ("cn.donting:plugin-spring-boot-starter:1.2.0.2-SNAPSHOT")
```
插件仅需第1步，主程序需要1，2两部


