package cn.donting.plugin.springboot.starter.application;

import cn.donting.plugin.springboot.starter.exception.PluginRuntimeException;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.springboot.plugin.top.PluginApplicationContext;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import cn.donting.springboot.plugin.top.PluginWebApplicationContext;
import lombok.Getter;

/**
 * 插件Application
 */
@Getter
public abstract class PluginApplication {

    public PluginApplication(PluginURLClassLoader pluginURLClassLoader, Class<?> mainClass, PluginInfo pluginInfo) {
        this.pluginURLClassLoader = pluginURLClassLoader;
        this.mainClass = mainClass;
        this.pluginInfo = pluginInfo;
    }

    /**
     * 插件的类加载器
     */
    protected final PluginURLClassLoader pluginURLClassLoader;
    /**
     * 插件的上下文
     */
    protected PluginApplicationContext pluginApplicationContext;
    /**
     * 插件主类
     */
    protected final Class<?> mainClass;
    /**
     * 插件信息
     */
    protected PluginInfo pluginInfo;


    /**
     * 运行插件
     *
     * @return PluginApplicationContext
     * @throws Exception 启动异常
     */
    public abstract PluginApplicationContext run(String[] args) throws Exception;

    /**
     * 停止插件
     *
     * @return 退出码
     * @throws Exception 停止异常
     */
    public abstract int stop() throws Exception;

    /**
     * 是否是web 插件
     *
     * @return true false
     * @throws PluginRuntimeException 插件未启动
     */
    public final boolean isWebPluginApplication() {
        if (pluginApplicationContext == null) {
            throw new PluginRuntimeException("pluginApplicationContext==null,插件未启动");
        } else {
            if (pluginApplicationContext instanceof PluginWebApplicationContext) {
                return true;
            }
        }
        return false;
    }

}
