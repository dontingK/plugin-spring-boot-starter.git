package cn.donting.plugin.springboot.starter.extension.lmp;

import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.extension.IPluginInfoLoader;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.net.URL;

/**
 * 默认实现的  IPluginInfoLoader
 * @author donting
 */
@Slf4j
public class DefaultPluginInfoLoader implements IPluginInfoLoader {
    @Override
    public PluginInfo get(Class<?> mainClass, PluginURLClassLoader pluginURLClassLoader, File file) throws PluginException {
        URL resource = pluginURLClassLoader.getResource("plugin.json");
        if(resource==null){
           throw new PluginException("not found : pluginInfo.json");
        }
        ObjectMapper objectMapper=new ObjectMapper();
        try {
            PluginInfo pluginInfo = objectMapper.readValue(resource, PluginInfo.class);
            if(pluginInfo.getId()==null || pluginInfo.getName()==null || pluginInfo.getVersion()==null || pluginInfo.getNumberVersion()==0){
                throw new PluginException("pluginInfo null Value:" +pluginInfo.toString() );
            }
            return pluginInfo;
        } catch (IOException e) {
            log.error(e.getMessage(),e);
            throw new PluginException(e.getMessage(),e);
        }
    }
}
