package cn.donting.plugin.main.test;


import cn.donting.plugin.springboot.starter.condition.OnPluginCondition;
import cn.donting.plugin.springboot.starter.loader.boot.BootJarPluginLoader;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;

import javax.servlet.ServletContainerInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.ServiceLoader;

/**
 * @author donting
 */
@SpringBootApplication
public class TestMainApplication {
    public static void main(String[] args) {
        SpringApplication.run(TestMainApplication.class,args);
    }


}
