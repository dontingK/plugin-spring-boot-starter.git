package cn.donting.plugin.springboot.starter.application;

import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import cn.donting.springboot.plugin.top.web.PluginWebDispatcher;

/**
 * web插件
 */
public abstract class PluginWebApplication extends PluginApplication implements PluginWebDispatcher {

    public PluginWebApplication(PluginURLClassLoader pluginURLClassLoader, Class<?> mainClass, PluginInfo pluginInfo) {
        super(pluginURLClassLoader, mainClass, pluginInfo);
    }
}
