package cn.donting.plugin.springboot.starter.extension.lmp;

import cn.donting.plugin.springboot.starter.application.PluginApplication;
import cn.donting.plugin.springboot.starter.event.*;
import cn.donting.plugin.springboot.starter.extension.PluginInfoDb;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.loader.PluginLoader;
import cn.donting.plugin.springboot.starter.properties.PluginProperties;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.util.FileCopyUtils;
import org.springframework.util.StringUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.UUID;

@Slf4j
public class DefaultPluginManager extends AbsPluginManager {

    @Autowired
    private PluginInfoDb pluginInfoDb;
    @Autowired
    private PluginProperties pluginProperties;
    @Autowired
    private ApplicationContext applicationContext;

    @Override
    public synchronized PluginApplication install(File file) throws PluginException {
        PluginLoader pluginLoader = getPluginLoader(file);
        File target = new File(pluginProperties.getPluginsPath() + File.separator + UUID.randomUUID().toString() + getInstallFileExtension(file));
        PluginApplication pluginApplication = null;
        try {
            if (!target.getParentFile().exists()) {
                target.getParentFile().mkdirs();
            }
            Files.copy(file.toPath(), target.toPath());
            pluginApplication = pluginLoader.loader(target);
            String id = pluginApplication.getPluginInfo().getId();
            if (pluginInfoDb.get(id) != null) {
                throw new PluginException(pluginApplication.getPluginInfo() + "已安装。");
            }
            applicationContext.publishEvent(new BeforeInstallEvent(pluginApplication.getPluginInfo()));

            PluginInfo pluginInfo = pluginApplication.getPluginInfo();
            pluginInfo.setInstallTime(System.currentTimeMillis());
            pluginInfo.setInstallFileName(target.getName());
            log.info("安装插件：id:[{}],name:[{}]", pluginInfo.getId(), pluginInfo.getName());
            pluginInfoDb.save(pluginInfo);

            applicationContext.publishEvent(new AfterInstallEvent(pluginApplication.getPluginInfo()));
        } catch (PluginException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new PluginException(ex.getMessage(), ex);
        }
        return pluginApplication;
    }

    @Override
    public synchronized void unInstall(String pluginId) throws PluginException {
        try {
            log.info("卸载：" + pluginId);
            if (pluginInfoDb.get(pluginId) == null) {
                throw new PluginException("为安装id为:" + pluginId + " 的插件");
            }
            PluginApplication pluginApplication = runPlugins.get(pluginId);
            if (pluginApplication != null) {
                stopPlugin(pluginId);
            }
            //删除安装信息
            PluginInfo delete = pluginInfoDb.delete(pluginId);
            applicationContext.publishEvent(new BeforeUninstallEvent(delete));
            //删除安装的文件
            new File(pluginProperties.getPluginsPath() + File.separator + delete.getInstallFileName()).delete();
            applicationContext.publishEvent(new AfterUninstallEvent(delete));
            log.info("卸载完成：{}[{}]", delete.getName(), delete.getId());
        } catch (PluginException ex) {
            throw new PluginException("卸载失败", ex);
        } catch (Exception ex) {
            throw new PluginException("卸载失败", ex);
        }
    }

    @Override
    public synchronized PluginApplication startPlugin(String pluginId) throws PluginException {
        try {
            PluginInfo pluginInstallInfo = pluginInfoDb.get(pluginId);
            log.info("启动插件：id:[{}],name:[{}]", pluginInstallInfo.getId(), pluginInstallInfo.getName());
            String installFilePath = getInstallFilePath(pluginInstallInfo);
            PluginLoader pluginLoader = getPluginLoader(new File(installFilePath));
            PluginApplication pluginApplication = pluginLoader.loader(new File(installFilePath));
            pluginApplication.run(null);
            super.runPlugins.put(pluginId, pluginApplication);
            return pluginApplication;
        } catch (PluginException ex) {
            throw ex;
        } catch (Exception ex) {
            throw new PluginException("启动失败", ex);
        }
    }

    @Override
    public synchronized int stopPlugin(String pluginId) throws PluginException {
        try {
            log.info("插件停止 id:" + pluginId);
            PluginApplication remove = runPlugins.remove(pluginId);
            if (remove == null) {
                log.info("插件未运行");
                return 0;
            }
            log.info("插件停止:{}[{}]", remove.getPluginInfo().getName(), remove.getPluginInfo().getId());
            int stop = remove.stop();
            log.info("插件停止exit code[]：[{}],name:[{}]", stop, remove.getPluginInfo().getId(), remove.getPluginInfo().getName());
            return stop;
        } catch (PluginException ex) {
            throw new PluginException("停止 异常", ex);
        } catch (Exception ex) {
            throw new PluginException("停止 异常", ex);
        }
    }

    @Override
    public synchronized PluginApplication updatePlugin(File file,boolean version) throws PluginException {
        PluginLoader pluginLoader = getPluginLoader(file);
        PluginApplication plugin = pluginLoader.loader(file);
        String pluginId = plugin.getPluginInfo().getId();

        PluginInfo pluginInstallInfo = pluginInfoDb.get(pluginId);
        if (pluginInstallInfo == null) {
            throw new PluginException("未安装id为:" + pluginId + " 的插件");
        }
        if (isRunning(pluginId)) {
            stopPlugin(pluginId);
        }
        log.info("更新插件：[{}:{}],是否校验版本:[{}]",pluginInstallInfo.getName(),pluginInstallInfo.getId(),version);

        applicationContext.publishEvent(new BeforeUpdateEvent(pluginInstallInfo));
        if (!plugin.getPluginInfo().getId().equals(pluginId)) {
            throw new PluginException("安装插件 id 不匹配：" + pluginId + "!=" + plugin.getPluginInfo().getId());
        }
        if (version && plugin.getPluginInfo().getNumberVersion() <= pluginInstallInfo.getNumberVersion()) {
            throw new PluginException("安装插件版本小于当前版本：" + plugin.getPluginInfo().getNumberVersion() + "<=" + pluginInstallInfo.getNumberVersion());
        }
        String installFilePath = getInstallFilePath(pluginInstallInfo);
        try {
            log.info("copy file to:" + installFilePath);
            FileCopyUtils.copy(file, new File(installFilePath));
        } catch (IOException e) {
            throw new PluginException("文件拷贝失败");
        }
        PluginInfo update = plugin.getPluginInfo();
        update.setUpdateTime(System.currentTimeMillis());
        update.setInstallTime(pluginInstallInfo.getInstallTime());
        update.setInstallFileName(new File(installFilePath).getName());
        log.info("update :" + update);
        pluginInfoDb.update(update);
        log.info("更新成功:{}[{}]", update.getName(), update.getId());
        applicationContext.publishEvent(new AfterUpdateEvent(update));
        return plugin;
    }

    protected String getInstallFileExtension(File file) {
        if(file.getName().endsWith(".classpath.dev")){
            return ".classpath.dev";
        }
        String filenameExtension = StringUtils.getFilenameExtension(file.getName());
        return "." + filenameExtension;
    }

    private String getInstallFilePath(PluginInfo pluginInstallInfo) {
        return pluginProperties.getPluginsPath() + File.separator + pluginInstallInfo.getInstallFileName();
    }
}
