package cn.donting.springboot.gradle.plugin;

import cn.donting.springboot.gradle.plugin.extension.PluginExtension;
import cn.donting.springboot.gradle.plugin.task.*;
import groovy.lang.Closure;
import org.gradle.api.Action;
import org.gradle.api.Plugin;
import org.gradle.api.Project;
import org.gradle.api.Task;
import org.gradle.api.artifacts.dsl.ComponentMetadataHandler;
import org.gradle.api.artifacts.dsl.ComponentModuleMetadataHandler;
import org.gradle.api.artifacts.dsl.DependencyConstraintHandler;
import org.gradle.api.artifacts.dsl.DependencyHandler;
import org.gradle.api.artifacts.type.ArtifactTypeContainer;
import org.gradle.api.artifacts.type.ArtifactTypeDefinition;
import org.gradle.api.attributes.Attribute;
import org.gradle.api.attributes.AttributeContainer;
import org.gradle.api.file.FileCollection;
import org.gradle.api.internal.tasks.DefaultSourceSetContainer;
import org.gradle.api.invocation.Gradle;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.api.plugins.ExtensionsSchema;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.reflect.TypeOf;
import org.gradle.api.specs.Spec;
import org.gradle.api.tasks.SourceSet;
import org.gradle.api.tasks.TaskContainer;

import java.io.File;
import java.util.*;

public class TaskRegister implements Plugin<Project> {
    private Project project;

    @Override
    public void apply(Project project) {
        this.project = project;
        try {
            this.registerPluginExtension();

            project.afterEvaluate(new Action<Project>() {
                @Override
                public void execute(Project project) {
                    JavaPluginConvention javaPluginConvention = javaPluginConvention(project);
                    Set<File> main = javaPluginConvention.getSourceSets().getByName("main").getRuntimeClasspath().getFiles();
                    boolean isMain = main.stream().anyMatch(file ->
                            file.getName().contains("plugin-spring-boot-starter"));
                    if (isMain) {
                        registerPluginMainTask();
                    } else {
                        registerPluginTask();
                    }
                }
            });


        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void registerPluginMainTask() {
        this.registerPluginMainRunTask();
        this.registerPluginMainJarTask();
        this.registerPluginMainWarTask();
    }

    private void registerPluginTask() {
        PluginRun pluginRun = project.getTasks().create("pluginMainRun", PluginRun.class, (plugin) -> {

        });
        pluginRun.setGroup("plugin");
        pluginRun.dependsOn("build");
        PluginClasspathTask pluginClasspath = project.getTasks().create("pluginClasspath", PluginClasspathTask.class, (plugin) -> {
        });
        pluginClasspath.setGroup("plugin");
        pluginClasspath.dependsOn("build");
        pluginRun.dependsOn(pluginClasspath);
    }


    private void registerPluginMainRunTask() {
        PluginExtension pluginExtension = project.getExtensions().findByType(PluginExtension.class);
        PluginMainRun pluginRun = project.getTasks().create("pluginMainRun", PluginMainRun.class, (pluginMainRun) -> {
            try {
                JavaPluginConvention javaPluginConvention = javaPluginConvention(project);

                FileCollection runtimeClasspath = javaPluginConvention.getSourceSets().getByName("main").getRuntimeClasspath();

                pluginMainRun.setMain("cn.donting.springboot.plugin.run.Main");
                ArrayList<String> libs = pluginMainRun.commonLibTempPath();
                runtimeClasspath = runtimeClasspath.plus(project.files(libs.toArray()));
                List<String> commonLib = pluginExtension.getCommonLib().getOrElse(new ArrayList<String>());
                commonLib=new ArrayList<>(commonLib);
                commonLib.add("plugin-sdk-spring-boot-starter");
                List<String> finalCommonLib = commonLib;
                //设置过滤器
                runtimeClasspath=  runtimeClasspath.filter(new Spec<File>() {
                    @Override
                    public boolean isSatisfiedBy(File file) {
                        if (libs.contains(file.getPath())) {
                            return true;
                        }
                        for (String name : finalCommonLib) {
                            if (file.getName().contains(name)) {
                                return true;
                            }
                        }
                        return false;
                    }
                });
                //设置 spring-boot-plugin-run 的classpath
                //spring-boot-plugin-run 在根据 参数 args 启动正真的 main
                pluginMainRun.setClasspath(runtimeClasspath);
            } catch (Exception ex) {
                throw new RuntimeException(ex);
            }
        });
        pluginRun.setGroup("plugin");
        pluginRun.dependsOn("build");


        project.getGradle().projectsEvaluated(new Action<Gradle>() {
            @Override
            public void execute(Gradle gradle) {
                //为主程序添加 插件的编译依赖
                Collection<Project> projects = project.getRootProject().getChildProjects().values();
                for (Project childProject : projects) {
                    if (childProject.equals(project)) {
                        continue;
                    }
                    try {
                        Task pluginClasspath = childProject.getTasks().getByName("pluginClasspath");
                        System.out.println("find pluginClasspath project:" + childProject.getName());
                        pluginRun.dependsOn(pluginClasspath);
                    } catch (Exception ex) {
                    }
                }
            }
        });

    }

    private void registerPluginMainJarTask() {
        PluginMainJar pluginMainJar = project.getTasks().create("pluginMainJar", PluginMainJar.class, (mainJar) -> {
        });
        pluginMainJar.setGroup("plugin");
        pluginMainJar.dependsOn("bootJar");
    }

    private void registerPluginMainWarTask() {
        try {
            if (project.getTasks().getByName("bootWar") == null) {
                return;
            }
        } catch (Exception ex) {
            return;
        }
        PluginMainWar pluginMainWar = project.getTasks().create("pluginMainWar", PluginMainWar.class, (mainWar) -> {
        });
        pluginMainWar.setGroup("plugin");
        pluginMainWar.dependsOn("bootWar");
    }

    private JavaPluginConvention javaPluginConvention(Project project) {
        return project.getConvention().getPlugin(JavaPluginConvention.class);
    }

    private void registerPluginExtension() {
        project.getExtensions().create("pluginStarter", PluginExtension.class, project);
    }


}
