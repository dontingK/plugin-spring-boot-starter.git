package cn.donting.plugin.springboot.starter.event;

import cn.donting.plugin.springboot.starter.entity.PluginInfo;

public class BeforeInstallEvent extends PluginEvent {
    public BeforeInstallEvent(PluginInfo pluginInfo) {
        super(pluginInfo);
    }
}
