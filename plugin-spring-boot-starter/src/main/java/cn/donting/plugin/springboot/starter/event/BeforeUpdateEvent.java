package cn.donting.plugin.springboot.starter.event;

import cn.donting.plugin.springboot.starter.entity.PluginInfo;

public class BeforeUpdateEvent extends PluginEvent {
    public BeforeUpdateEvent(PluginInfo pluginInfo) {
        super(pluginInfo);
    }
}
