package cn.donting.springboot.gradle.plugin.task;

import cn.donting.springboot.gradle.plugin.ClasspathUtil;
import cn.donting.springboot.gradle.plugin.extension.PluginExtension;
import cn.donting.springboot.gradle.plugin.util.Utils;
import org.apache.tools.ant.taskdefs.Java;
import org.gradle.api.Project;
import org.gradle.api.file.FileCollection;
import org.gradle.api.internal.file.collections.FileCollectionAdapter;
import org.gradle.api.internal.file.collections.FileTreeAdapter;
import org.gradle.api.internal.tasks.DefaultSourceSet;
import org.gradle.api.internal.tasks.DefaultSourceSetContainer;
import org.gradle.api.plugins.ExtensionContainer;
import org.gradle.api.plugins.JavaPluginConvention;
import org.gradle.api.provider.ListProperty;
import org.gradle.api.provider.Property;
import org.gradle.api.tasks.Internal;
import org.gradle.api.tasks.JavaExec;
import org.gradle.api.tasks.SourceSet;
import org.springframework.boot.gradle.tasks.run.BootRun;

import java.io.*;
import java.net.URL;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * main run
 */
public class PluginMainRun extends JavaExec {
    /**
     * dve插件 classpath 目录
     */
    public static final String PLUGIN_DEV_DIR = "--cn.donting.plugin.dev.dir=";
    public static final String OPEN_DEV = "--cn.donting.plugin.dev.enabled=true";

    public static final String PREFIX_CLASSPATH = "--classpath=";
    public static final String PREFIX_MAIN_CLASS = "--mainClass=";

    public PluginMainRun() {
    }

    @Override
    public void exec() {
        ArrayList<String> args = new ArrayList<>();
        try {

            args.add("--spring.output.ansi.console-available=true");
            //开启spring boot 的彩色日志
            args.add("--spring.output.ansi.enabled=ALWAYS");
            //dev模式
            args.add(OPEN_DEV);
            args.add(getPluginDevDir(getProject()));
            args.add("--mainClass=" + Utils.springbootMainClass(getProject()));
            args.add("--classpath=" + getMainClasspath());
            setArgs(args);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        try {
            super.exec();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    /**
     * 获取 主程序 classpath
     *
     * @return --classpath=1;2;3;4
     */
    @Internal
    public String getMainClasspath() {
        Project project = getProject();

        PluginExtension pe = project.getExtensions().findByType(PluginExtension.class);
        List<String> commonLib = pe.getCommonLib().getOrElse(new ArrayList<>());

        JavaPluginConvention javaPluginConvention = javaPluginConvention(project);
        FileCollection runtimeClasspath = javaPluginConvention.getSourceSets().getByName("main").getRuntimeClasspath();
        Set<File> files = runtimeClasspath.getFiles();

        List<File> classpathFile = files.stream().filter(new Predicate<File>() {
            @Override
            public boolean test(File file) {
                for (String libName : commonLib) {
                    if (file.getName().contains(libName)) {
                        System.out.println("公共包：" + file.getName());
                        return false;
                    }
                }
                return true;
            }
        }).collect(Collectors.toList());

        String classpath = "";

        for (int i = 0; i < classpathFile.size()-1; i++) {
            classpath=classpath+classpathFile.get(i).getPath()+";";
        }
        classpath=classpath+classpathFile.get(classpathFile.size()-1).getPath();
        return classpath;
    }

    private JavaPluginConvention javaPluginConvention(Project project) {
        return project.getConvention().getPlugin(JavaPluginConvention.class);
    }

    public static String getPluginDevDir(Project project) {
        File rootDir = project.getRootDir();
        return PLUGIN_DEV_DIR + rootDir.getPath();
    }

    public ArrayList<String> commonLibTempPath() throws IOException {
        System.out.println("释放 commonLib");
        ClassLoader classLoader = PluginMainRun.class.getClassLoader();
        URL inx = classLoader.getResource("commonLib/commonLib.idx");
        InputStream inputStream = inx.openStream();

        byte[] bytes = new byte[inputStream.available()];


        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) inputStream.read();
        }
        inputStream.close();
        String inxText = new String(bytes);
        String[] libs = inxText.split("\n");
        File buildDir = getProject().getBuildDir();
        File commonLib = new File(buildDir + File.separator + "commonLib");
        if (!commonLib.exists()) {
            commonLib.mkdirs();
        }
        ArrayList<String> libsFilePath = new ArrayList<>();
        for (int i = 0; i < libs.length; i++) {
            URL lib = classLoader.getResource("commonLib/" + libs[i]);
            File file = new File(commonLib.getPath() + File.separator + libs[i]);
            if (!file.exists()) {
                file.createNewFile();
            }
            bytes = readInputStream(lib.openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(bytes);
            fileOutputStream.flush();
            fileOutputStream.close();
            libsFilePath.add(file.getPath());
        }
        return libsFilePath;
    }

    public byte[] readInputStream(InputStream inputStream) throws IOException {
        byte[] bytes = new byte[inputStream.available()];
        for (int i = 0; i < bytes.length; i++) {
            bytes[i] = (byte) inputStream.read();
        }
        inputStream.close();
        return bytes;
    }

}
