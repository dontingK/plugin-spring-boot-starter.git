package cn.donting.plugin.springboot.starter.application;

import cn.donting.plugin.springboot.starter.exception.PluginNotSupportException;
import cn.donting.plugin.springboot.starter.loader.PluginURLClassLoader;
import cn.donting.springboot.plugin.top.PluginApplicationContext;
import cn.donting.plugin.springboot.starter.entity.PluginInfo;

/**
 * springboot  的 Application 非web
 */
public class BootApplication extends PluginApplication {


    public BootApplication(PluginURLClassLoader pluginURLClassLoader, Class<?> mainClass, PluginInfo pluginInfo) {
        super(pluginURLClassLoader, mainClass, pluginInfo);
    }

    @Override
    public PluginApplicationContext run(String[] args) throws Exception {
        throw new PluginNotSupportException("BootApplication(非web暂未支持)");
    }

    @Override
    public int stop() throws Exception {
        throw new PluginNotSupportException("BootApplication(非web暂未支持)");
    }

}
