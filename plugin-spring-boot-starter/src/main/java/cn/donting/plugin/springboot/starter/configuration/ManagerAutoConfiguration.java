package cn.donting.plugin.springboot.starter.configuration;

import cn.donting.plugin.springboot.starter.env.PluginDevProperty;
import cn.donting.plugin.springboot.starter.extension.lmp.AbsPluginManager;
import cn.donting.plugin.springboot.starter.extension.PluginInfoDb;
import cn.donting.plugin.springboot.starter.extension.lmp.JsonPluginInfoDb;
import cn.donting.plugin.springboot.starter.extension.lmp.DefaultPluginManager;
import cn.donting.plugin.springboot.starter.exception.PluginException;
import cn.donting.plugin.springboot.starter.properties.PluginProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Enumeration;



/**
 * 插件管理 配置类
 */
@Configuration
@Slf4j
@EnableConfigurationProperties({PluginDevProperty.class, PluginProperties.class})
@Order(Ordered.HIGHEST_PRECEDENCE)
public class ManagerAutoConfiguration {
    PluginProperties properties;

    public ManagerAutoConfiguration(PluginProperties properties) {
        this.properties = properties;
        try {
            initPluginLib();
        } catch (Exception ex) {
            throw new RuntimeException(ex);
        }
    }

    @Bean
    @ConditionalOnMissingBean(AbsPluginManager.class)
    public DefaultPluginManager pluginManager() {
        return new DefaultPluginManager();
    }

    @Bean
    @ConditionalOnMissingBean(PluginInfoDb.class)
    public JsonPluginInfoDb jsonPluginInfoDb(PluginProperties properties) {
        return new JsonPluginInfoDb(properties);
    }


    /**
     * 释放 插件依赖jar
     *
     * @throws Exception
     */
    private void initPluginLib() throws Exception {
        ClassLoader classLoader = LoaderAutoConfiguration.class.getClassLoader();
        //获取插件依赖 的 idx
        Enumeration<URL> resources = classLoader.getResources("META-INF/pluginLib.idx");
        //插件在 classLoader 中的路径
        ArrayList<String> pluginLibPath = new ArrayList<>();
        while (resources.hasMoreElements()) {
            URL url = resources.nextElement();
            InputStream inputStream = url.openStream();
            byte[] aByte = getByte(inputStream);
            String s = new String(aByte);
            String[] libPath = s.split("\n");
            for (String path : libPath) {
                if (pluginLibPath.contains(path)) {
                    log.error("pluginLibPath:{} 冲突.", path);
                    inputStream.close();
                    throw new PluginException("pluginLib " + path + "冲突");
                }
                pluginLibPath.add(path);
            }
            inputStream.close();
        }

        //释放 jar
        for (String path : pluginLibPath) {
            path = path.trim();
            URL resource = classLoader.getResource(path);
            if (resource == null) {
                log.error("not found:" + path);
                throw new PluginException(("not found:" + path));
            }
            InputStream inputStream = resource.openStream();
            byte[] aByte = getByte(inputStream);
            inputStream.close();
            String pluginsLibPath = getPluginsLibPath();
            File file = new File(pluginsLibPath + File.separator + path.replaceAll("/", "-"));
            file.getParentFile().mkdirs();
            file.createNewFile();
            FileOutputStream fileOutputStream = new FileOutputStream(file);
            fileOutputStream.write(aByte);
            fileOutputStream.close();
        }

    }

    /**
     * 从 inputStream 中读取 byte[]. 直接read(byte[]) ,最后3位byte 读取不了
     *
     * @param inputStream
     * @return
     * @throws IOException
     */
    public byte[] getByte(InputStream inputStream) throws IOException {
        byte[] bytes = new byte[inputStream.available()];

        int by;
        int index = 0;
        while ((by = inputStream.read()) != -1) {
            bytes[index++] = (byte) by;
        }
        return bytes;
    }

    private String getPluginsLibPath(){
        return properties.pluginLibPath();
    }
}
